# Copyright 2014  Lars Wirzenius and Codethink Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=


def load_shell_libraries(paths, pre_read_cb=lambda filename: None, open=open):
    '''Helper for loading shell libraries from files.

    `paths`: Iterable of file paths to shell libraries.
    `pre_read_cb`: Optional callback for providing progress reporting
                   and logging.
                   It is given only one parameter, of the path to the
                   shell library that is about to be loaded.
    `open`: Optional function for opening file paths. This is useful to
            override how files are opened for unit tests or virtual
            file systems.

    This function is provided so that when shell libraries are loaded
    by applications that use yarnlib, they can all load them with the
    same format, including the comments about where the snippet came from,
    and any future additions.

    '''

    libs = []
    for filename in paths:
        pre_read_cb(filename)
        with open(filename) as f:
            text = f.read()
        libs.append('# Loaded from %s\n\n%s\n\n' % (filename, text))

    return ''.join(libs)
