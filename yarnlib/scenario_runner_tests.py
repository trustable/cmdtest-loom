# Copyright 2014  Codethink Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=


import itertools
import unittest

import yarnlib


class ScenarioRunnerEnvironmentTests(unittest.TestCase):

    def setUp(self):
        self.cmdlog = []
        self.scenario = yarnlib.Scenario('foo')
        step = yarnlib.ScenarioStep('THEN', 'foo bar')
        step.implementation = yarnlib.Implementation('THEN', r'foo (\S+)',
                                                     'echo foo $MATCH_1')
        self.scenario.steps = [step]

    def fake_cmdrunner(self, cmd, env=None, cwd=None):
        self.cmdlog.append((cmd, env, cwd))
        return 0, '', ''

    def test_command_env(self):
        sr = yarnlib.ScenarioRunner('', '/tmp', {'FOO': 'bar'},
                                    cmdrunner=self.fake_cmdrunner)
        sr.run_scenario(self.scenario, '/tmp/datadir', '/tmp/home')
        cmd, cmd_env, cmd_cwd = self.cmdlog[0]
        self.assertIn('FOO', cmd_env)
        self.assertEqual(cmd_env['FOO'], 'bar')
        self.assertEqual(cmd_env['SRCDIR'], '/tmp')
        self.assertEqual(cmd_env['DATADIR'], '/tmp/datadir')
        self.assertEqual(cmd_env['HOME'], '/tmp/home')
        self.assertEqual(cmd_env['MATCH_1'], 'bar')
        self.assertEqual('/tmp', cmd_cwd)

    def test_command_args(self):
        prelude = 'TESTVAR=foo\n'
        impl_shell = self.scenario.steps[0].implementation.shell
        sr = yarnlib.ScenarioRunner(prelude, '/tmp',
                                    cmdrunner=self.fake_cmdrunner)
        sr.run_scenario(self.scenario, '/tmp/datadir', '/tmp/home')
        shell, opts, command = self.cmdlog[0][0]
        self.assertTrue(shell.endswith('sh'))
        self.assertTrue(command.startswith(prelude))
        self.assertIn(impl_shell, command)


class ScenarioRunnerFlowTests(unittest.TestCase):

    def setUp(self):
        self.cmdlog = []
        self.scenario_runner = (
            yarnlib.ScenarioRunner('', 'srcdir',
                                   cmdrunner=self.fake_cmdrunner))
        for verb, cmd in itertools.product(('ASSUMING', 'THEN', 'FINALLY'),
                                           ('true', 'false')):
            step = yarnlib.ScenarioStep(verb, cmd)
            step.implementation = yarnlib.Implementation(verb, cmd, cmd)
            setattr(self, '%s_%s' % (verb.lower(), cmd), step)

    def fake_cmdrunner(self, cmd, *args, **kwargs):
        self.cmdlog.append(cmd)
        if 'false' in cmd[-1]:
            return 1, '', ''
        return 0, '', ''

    def run_scenario(self, *steps):
        scenario = yarnlib.Scenario('foo')
        scenario.steps = steps
        return self.scenario_runner.run_scenario(scenario, 'data', 'home')

    def test_assuming_skips_remaining(self):
        ok = self.run_scenario(
            self.assuming_false,
            self.then_true,
            self.finally_true,
        )
        self.assertTrue(ok)
        self.assertEqual(len(self.cmdlog), 1)

    def test_cleanup_run_on_success(self):
        ok = self.run_scenario(
            self.assuming_true,
            self.then_true,
            self.finally_true,
        )
        self.assertTrue(ok)
        self.assertEqual(len(self.cmdlog), 3)

    def test_cleanup_run_on_failure(self):
        ok = self.run_scenario(
            self.then_false,
            self.finally_true,
        )
        self.assertFalse(ok)
        self.assertEqual(len(self.cmdlog), 2)

    def test_skip_steps_after_failure(self):
        ok = self.run_scenario(
            self.then_false,
            self.then_true,
        )
        self.assertFalse(ok)
        self.assertEqual(len(self.cmdlog), 1)

    def test_cleanup_failure(self):
        ok = self.run_scenario(
            self.then_true,
            self.finally_false,
            self.finally_true,
        )
        self.assertFalse(ok)
        self.assertEqual(len(self.cmdlog), 2)
