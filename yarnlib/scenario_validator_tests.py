# Copyright 2014  Codethink Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=


import unittest

import yarnlib


class ScenarioValidatorTests(unittest.TestCase):

    def setUp(self):
        pass

    def test_check_scenarios_fail(self):
        sv = yarnlib.ScenarioValidator([])
        with self.assertRaises(yarnlib.NoScenariosError):
            sv.check_there_are_scenarios()

    def test_check_scenarios_pass(self):
        sv = yarnlib.ScenarioValidator([
            yarnlib.Scenario('foo')
        ])
        sv.check_there_are_scenarios()

    def test_check_duplicate_names_fail(self):
        sv = yarnlib.ScenarioValidator([
            yarnlib.Scenario('foo'),
            yarnlib.Scenario('foo'),
        ])
        with self.assertRaises(yarnlib.DuplicateScenariosError):
            sv.check_for_duplicate_scenario_names()

    def test_check_duplicate_names_pass(self):
        sv = yarnlib.ScenarioValidator([
            yarnlib.Scenario('foo'),
            yarnlib.Scenario('bar'),
        ])
        sv.check_for_duplicate_scenario_names()

    def test_check_for_thens_fail(self):
        sv = yarnlib.ScenarioValidator([
            yarnlib.Scenario('foo')
        ])
        with self.assertRaises(yarnlib.NoThensError):
            sv.check_for_thens()

    def test_check_for_thens_pass(self):
        scenario = yarnlib.Scenario('foo')
        scenario.steps = [
            yarnlib.ScenarioStep('THEN', 'badger stoat')
        ]
        sv = yarnlib.ScenarioValidator([scenario])
        sv.check_for_thens()
