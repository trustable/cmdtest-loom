# Copyright 2014  Lars Wirzenius and Codethink Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=


import cliapp
import collections


class NoScenariosError(cliapp.AppException):

    def __init__(self):
        cliapp.AppException.__init__(
            self, 'There are no scenarios; must have at least one.')


class DuplicateScenariosError(cliapp.AppException):

    def __init__(self, duplicates):
        duplist = ''.join('  %s\n' % name for name in duplicates)
        cliapp.AppException.__init__(
            self, 'There are scenarios with duplicate names:\n%s' % duplist)


class NoThensError(cliapp.AppException):

    def __init__(self, thenless):
        cliapp.AppException.__init__(
            self,  'Some scenarios have no THENs:\n%s' %
                   ''.join('  "%s"\n' % s.name for s in thenless))


class ScenarioValidator(object):

    def __init__(self, scenarios):
        self.scenarios = scenarios

    def validate_all(self): # pragma: no cover
        '''Convenience method to run all checks.'''
        self.check_there_are_scenarios()
        self.check_for_duplicate_scenario_names()
        self.check_for_thens()

    def check_there_are_scenarios(self):
        '''Check whether any scenarios were found.'''
        if not self.scenarios:
            raise NoScenariosError()

    def check_for_duplicate_scenario_names(self):
        '''Check whether multiple scenarios were found sharing a name.'''
        counts = collections.Counter()
        for s in self.scenarios:
            counts[s.name] += 1

        duplicates = [name for name in counts if counts[name] > 1]
        if duplicates:
            raise DuplicateScenariosError(duplicates)

    def check_for_thens(self):
        no_thens = []
        for scenario in self.scenarios:
            for step in scenario.steps:
                if step.what == 'THEN':
                    break
            else:
                no_thens.append(scenario)

        if no_thens:
            raise NoThensError(no_thens)
