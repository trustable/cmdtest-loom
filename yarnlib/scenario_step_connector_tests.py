# Copyright 2014  Codethink Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=


import unittest

import yarnlib


class ImplementsMatchesTests(unittest.TestCase):

    def test_match(self):
        i = yarnlib.Implementation('THEN', r'foo', 'echo foo\n')
        step = yarnlib.ScenarioStep('THEN', 'foo')
        self.assertTrue(yarnlib.implements_matches_step(i, step))

    def test_no_match(self):
        i = yarnlib.Implementation('THEN', r'foo', 'echo foo\n')
        step = yarnlib.ScenarioStep('THEN', 'bar')
        self.assertIs(yarnlib.implements_matches_step(i, step), None)

    def test_not_match_all(self):
        i = yarnlib.Implementation('THEN', r'foo', 'echo foo\n')
        step = yarnlib.ScenarioStep('THEN', 'foo bar')
        self.assertIs(yarnlib.implements_matches_step(i, step), None)


class ScenarioStepConnectorTests(unittest.TestCase):

    def setUp(self):
        pass

    def test_all_implemented(self):
        impl = yarnlib.Implementation('THEN', r'foo', 'echo foo\n')
        scenario = yarnlib.Scenario('foo')
        scenario.steps = [
            yarnlib.ScenarioStep('THEN', 'foo')
        ]
        ssc = yarnlib.ScenarioStepConnector([impl])
        scenarios = ssc.connect_implementations([scenario])
        self.assertIs(scenarios[0].steps[0].implementation, impl)

    def test_multiple_impls(self):
        ssc = yarnlib.ScenarioStepConnector([
            yarnlib.Implementation('THEN', r'foo \S+', 'echo foo\n'),
            yarnlib.Implementation('THEN', r'\S+ bar', 'echo bar\n'),
        ])
        scenario = yarnlib.Scenario('foo bar')
        scenario.steps = [
            yarnlib.ScenarioStep('THEN', 'foo bar')
        ]
        with self.assertRaises(yarnlib.StepMultipleImplementationsError):
            scenarios = ssc.connect_implementations([scenario])

    def test_noimpl_fail(self):
        ssc = yarnlib.ScenarioStepConnector([
            yarnlib.Implementation('THEN', r'foo', 'echo foo\n'),
        ])
        scenarios = []
        for scenario_name, step_text in [('foo', 'foo'), ('bar', 'bar')]:
            scenario = yarnlib.Scenario(scenario_name)
            scenario.steps = [
                yarnlib.ScenarioStep('THEN', step_text),
            ]
            scenarios.append(scenario)
        with self.assertRaises(yarnlib.StepNotImplementedError):
            scenarios = ssc.connect_implementations(scenarios)

    def test_noimpl_ignore(self):
        ssc = yarnlib.ScenarioStepConnector([
            yarnlib.Implementation('THEN', r'foo', 'echo foo\n'),
        ], lambda *args: True)
        all_scenarios = []
        for scenario_name, step_text in [('foo', 'foo'), ('bar', 'bar')]:
            scenario = yarnlib.Scenario(scenario_name)
            scenario.steps = [
                yarnlib.ScenarioStep('THEN', step_text),
            ]
            all_scenarios.append(scenario)
        implemented_scenarios = ssc.connect_implementations(all_scenarios)
        self.assertTrue(implemented_scenarios)
