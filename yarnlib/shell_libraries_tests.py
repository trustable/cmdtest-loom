# Copyright 2014  Codethink Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-3+ =*=


import contextlib
import StringIO
import unittest

import yarnlib


class ShellLibrariesTests(unittest.TestCase):

    libfiles = {
        'die.shlib': '''
        die() {
            echo "$@" >&2
            return 1
        }
        ''',
        'chdir.shlib':'''
        chdir() {
            (
                cd "$1"
                shift
                exec "$@"
            )
        }
        ''',
    }

    def fakeopen(self, path, *args, **kwargs):
        return contextlib.closing(StringIO.StringIO(self.libfiles[path]))

    def test_file_names_listed(self):
        prelude = yarnlib.load_shell_libraries(self.libfiles.keys(),
                                               open=self.fakeopen)
        for filename in self.libfiles.iterkeys():
            self.assertIn(filename, prelude)

    def test_all_contents_included(self):
        prelude = yarnlib.load_shell_libraries(self.libfiles.keys(),
                                               open=self.fakeopen)
        for contents in self.libfiles.itervalues():
            self.assertIn(contents, prelude)
